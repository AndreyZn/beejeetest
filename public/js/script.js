$('body').on('click', '.change-status-link', function(e){

    e.preventDefault();
    let id = $(this).data('id')

    $.ajax({
        url: '/task/update',
        data: {id: id},
        type: 'GET',
        success: function(res){
            $('#ch-status-'+id).prop('checked', true);
        },
        error: function(){
            alert('Ошибка! Попробуйте позже');
        }
    });
});