<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Error</title>
</head>
<body>
<div>An error has occurred</div>
<p><b>Type error:</b><?=$errno;?></p>
<p><b>Text error:</b><?=$errstr;?></p>
<p><b>File error:</b><?=$errfile;?></p>
<p><b>Line error:</b><?=$errline;?></p>
</body>
</html>