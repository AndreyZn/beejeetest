<?php


namespace src;

/**
 * Trait TSingleton
 *
 * @package src
 */
trait TSingleton {

  /**
   * @var $instance - object
   */
  private static $instance;

  /**
   * @return \src\TSingleton
   */
  public static function instance() {
    if (self::$instance === NULL) {
      self::$instance = new self;
    }

    return self::$instance;
  }

}