<?php


namespace src\Controller;


use src\Model\UserModel;

/**
 * Class UserController
 *
 * @package src\Controller
 */
class UserController extends BaseController {

  /**
   * Registration
   *
   * @throws \RedBeanPHP\RedException\SQL
   */
  public function signupAction() {
    $this->layout = 'signup';

    if (!empty($_POST)) {
      $user = new UserModel();
      $data = $_POST;
      $user->load($data);

      if (!$user->validate($data) || !$user->checkUnique()) {
        $user->getErrors();
        $_SESSION['form_data'] = $data;
      }
      else {
        $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
        if ($user->save('users')) {
          $_SESSION['success'] = 'Вы успешно зарегистрировались!';
        }
        else {
          $_SESSION['error'] = 'Ошибка записи в БД!';
        }
      }
      redirect(PATH);
    }
    $this->setMeta('Регистрация');
  }

  /**
   * Autorization
   */
  public function loginAction() {
    $this->layout = 'signup';

    if (!empty($_POST)) {
      $user = new UserModel();
      if ($user->login()) {
        $_SESSION['success'] = 'Вы успешно авторизованы';
        redirect(PATH);
      }
      else {
        $_SESSION['error'] = 'Логин/пароль введены неверно';
        redirect();
      }
    }

    $this->setMeta('Вход');
  }

  /**
   * Logout user
   */
  public function logoutAction() {
    if (isset($_SESSION['user'])) {
      unset($_SESSION['user']);
    }
    redirect();
  }

}