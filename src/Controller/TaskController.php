<?php

namespace src\Controller;

use RedBeanPHP\R;
use src\Model\TaskModel;

/**
 * Class TaskController
 *
 * @package src\Controller
 */
class TaskController extends BaseController {

  /**
   * Add task
   *
   * @throws \RedBeanPHP\RedException\SQL
   */
  public function addAction() {
    $this->layout = 'signup';

    if (!empty($_POST)) {
      $task = new TaskModel();
      $data = $_POST;
      $task->load($data);

      if (!$task->validate($data)) {
        $task->getErrors();
      }
      else {
        if (isset($_SESSION['user'])) {
          $task->attributes['user_id'] = $task->getUserId();
          $task->attributes['user_name'] = $_SESSION['user']['name'];
          $task->attributes['email'] = $_SESSION['user']['email'];
        }
        else {
          $task->attributes['user_id'] = 0;
          $task->attributes['user_name'] = isset($data['user_name']) ? $data['user_name'] : 'Guest';
          $task->attributes['email'] = isset($data['email']) ? $data['email'] : 'guest@gmail.com';;
        }

        if ($task->save('tasks')) {
          $_SESSION['success'] = 'Задание успешно создано!';
        }
        else {
          $_SESSION['error'] = 'Ошибка записи в БД!';
        }
      }
      redirect();
    }
    $this->setMeta('Создание нового задания');
  }

  /**
   * Edit task
   *
   * @throws \RedBeanPHP\RedException\SQL
   */
  public function editAction() {
    $this->layout = 'signup';

    if (!$_SESSION['user']) {
      $_SESSION['error'] = 'Вы должны авторизироваться!';
      redirect('/user/login');
    }

    if (!empty($_GET['id'])) {
      $id = $_GET['id'];
      $task = R::load('tasks', $id);
      $textOld = $task->text;

      $this->set(compact('textOld'));
    }

    if (!empty($_POST)) {
      $model = new TaskModel();
      $data = $_POST;
      $model->load($data);

      if (!$model->validate($data)) {
        $model->getErrors();
      }
      else {
        if ($textOld != $data['text']) {
          $task->text = $data['text'];
          $task->update_adm = 'Отредактировано администратором';
          R::store($task);
          $_SESSION['success'] = 'Изменения сохранены!';
        }
      }
      redirect(PATH);
    }
  }

  /**
   * Checked status task (Ajax)
   *
   * @throws \RedBeanPHP\RedException\SQL
   */
  public function updateAction() {
    $id = !empty($_GET['id']) ? (int) $_GET['id'] : NULL;
    $task = R::load('tasks', $id);
    $task->status = 'выполнено';
    R::store($task);
  }

}