<?php


namespace src\Controller;

use src\Model\BaseModel;
use src\View;

/**
 * Class BaseController
 *
 * @package src\Controller
 */
abstract class BaseController {

  public $route;

  public $controller;

  public $model;

  public $view;

  public $prefix;

  public $layout;

  public $data = [];

  public $meta = ['title' => '', 'desc' => '', 'keywords' => ''];

  /**
   * BaseController constructor.
   *
   * @param $route
   */
  public function __construct($route) {
    $this->route = $route;
    $this->controller = $route['controller'];
    $this->model = $route['controller'];
    $this->view = $route['action'];
    $this->prefix = $route['prefix'];

    new BaseModel();
  }

  /**
   * Display view
   *
   * @throws \Exception
   */
  public function getView() {
    $viewObject = new View($this->route, $this->layout, $this->view, $this->meta);
    $viewObject->render($this->data);
  }

  /**
   * Data transfer to view
   *
   * @param $data
   */
  public function set($data) {
    $this->data = $data;
  }

  /**
   * Set metatag
   *
   * @param string $title
   * @param string $desc
   * @param string $keywords
   */
  public function setMeta($title = '', $desc = '', $keywords = '') {
    $this->meta['title'] = $title;
    $this->meta['desc'] = $desc;
    $this->meta['keywords'] = $keywords;
  }

}