<?php


namespace src\Controller;


use RedBeanPHP\R;
use src\App;
use src\Pagination;

/**
 * Class MainController
 *
 * @package src\Controller
 */
class MainController extends BaseController {

  /**
   * Main page
   */
  public function indexAction() {
    $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $perpage = App::$app->getProperty('pagination');
    $total = R::count('tasks');
    $pagination = new Pagination($page, $perpage, $total);
    $start = $pagination->getStart();

    $sort1 = isset($_GET['optradio0']) ? $_GET['optradio0'] : 'ASK';
    $sort2 = isset($_GET['optradio1']) ? $_GET['optradio1'] : 'id';

    if (isset($_GET['optradio0']) || isset($_GET['optradio1'])) {
      $sql = " ORDER BY $sort2 $sort1 LIMIT $start, $perpage";
      $tasks = R::find('tasks', $sql);
    } else {
      $sql = "LIMIT $start, $perpage";
      $tasks = R::find('tasks', $sql);
    }

    $this->setMeta('Main page', 'Description...', 'Keywords...');
    $this->set(compact('tasks', 'pagination'));
  }

}