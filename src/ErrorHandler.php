<?php


namespace src;

/**
 * Class ErrorHandler
 *
 * @package src
 */
class ErrorHandler {

  /**
   * ErrorHandler constructor.
   */
  public function __construct() {
    if (DEBUG) {
      error_reporting(E_ERROR | E_WARNING | E_PARSE);
    }
    else {
      error_reporting(0);
    }

    set_exception_handler([$this, 'exceptionHandler']);
  }

  /**
   * @param $e
   */
  public function exceptionHandler($e) {
    $this->logErrors($e->getMessage(), $e->getFile(), $e->getLine());
    $this->displayError('Exception', $e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode());
  }

  /**
   * @param string $message
   * @param string $file
   * @param string $line
   */
  protected function logErrors($message = '', $file = '', $line = '') {
    error_log("[" . date('Y-m-d H:i:s') . "] Text error: {$message} | File: {$file} | Line: {$line} \n======\n", 3, ROOT . '/tmp/errors.log');
  }

  /**
   * @param $errno
   * @param $errstr
   * @param $errfile
   * @param $errline
   * @param int $responce
   */
  protected function displayError($errno, $errstr, $errfile, $errline, $responce = 404) {
    http_response_code($responce);
    if ($responce == 404 && !DEBUG) {
      require WWW . '/errors/404.php';
      die;
    }

    if (DEBUG) {
      require WWW . '/errors/dev.php';
    }
    else {
      require WWW . '/errors/prod.php';
    }
    die;
  }

}