<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <!-- Bootstrap -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <script src="/js/jquery-3.5.0.min.js"></script>
  <?= $this->getMeta(); ?>
</head>
<body>
<header>
  <div class="container">
    <nav class="navbar navbar-light bg-faded">
      <div class="navbar-brand">Задачник</div>
      <ul class="nav navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="/">Главная <span
              class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/task/add">Добавить задачу</a>
        </li>
        <?php if (!empty($_SESSION['user'])): ?>
          <li class="nav-item">
            <a class="nav-link" href="/user/logout">Выйти</a>
          </li>
        <?php else: ?>
          <li class="nav-item">
            <a class="nav-link" href="/user/signup">Зарегистрироваться</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/user/login">Войти</a>
          </li>
        <?php endif; ?>
      </ul>
    </nav>
    <?php if (!empty($_SESSION['user'])): ?>
      <p class="">Добро пожаловать, <?= $_SESSION['user']['name'] ?></p>
    <?php endif; ?>
  </div>
</header>
<section>
  <div class="container">
    <div class="row">
      <main class="col-md-8 col-lg-9">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <?php if ($_SESSION['error']): ?>
                <div class="alert alert-danger">
                  <?php echo $_SESSION['error'];
                  unset($_SESSION['error']); ?>
                </div>
              <?php endif; ?>
              <?php if ($_SESSION['success']): ?>
                <div class="alert alert-success">
                  <?php echo $_SESSION['success'];
                  unset($_SESSION['success']); ?>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <?= $content; ?>
      </main>
      <!-- Aside -->
      <aside class="col-mf-4 col-md-3">
        <form id="sort" action="/main/index" method="GET">
          <div class="thumbnail">
            <div class="caption">
              <div id="legend">
                <legend class="">Сортировать</legend>
              </div>
              <div class="sort">
                <div class="radio">
                  <label><input type="radio" name="optradio0" value='ASC'
                                checked>По возрастанию</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="optradio0" value='DESC'>По
                    убыванию</label>
                </div>
              </div>
              <div class="sort">
                <div class="radio">
                  <label><input type="radio" name="optradio1" value='user_name'>По
                    пользователю</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="optradio1" value='email'>По
                    email</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="optradio1" value='status'>По
                    статусу</label>
                </div>
              </div>
              <button type="submit" class="btn btn-primary">
                Сортировать
              </button>
            </div>
          </div>
        </form>
      </aside>
    </div>
  </div>
</section>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
  src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/script.js">

</script>

</body>
</html>
