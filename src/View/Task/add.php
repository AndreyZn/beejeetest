<form class="form-horizontal col-md-8" id="add_task" data-toggle="validator"
      role="form" action='' method="POST">
  <div id="legend">
    <legend class="">Создать новую задачу</legend>
  </div>
  <?php if (!$_SESSION['user']): ; ?>
    <div class="form-group has-feedback">
      <!-- Username -->
      <label class="control-label" for="name">Имя</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input type="text" id="user_name" name="user_name" placeholder="Имя"
               class="form-control" data-maxlength="255" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
      <!-- E-mail -->
      <label class="control-label" for="email">E-mail</label>
      <div class="input-group">
        <span class="input-group-addon"><i
            class="glyphicon glyphicon-envelope"></i></span>
        <input type="email" id="email" name="email" placeholder="email"
               class="form-control" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="help-block with-errors"></div>
    </div>
  <?php endif; ?>
  <div class="form-group has-feedback">
    <!-- E-mail -->
    <label class="control-label" for="email">Текст задачи</label>
    <div class="input-group">
      <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
      <textarea class="form-control" id="text" name="text"
                placeholder="Введите текст" rows="7" cols="60"
                required></textarea>
      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
    </div>
    <div class="help-block with-errors"></div>
  </div>
  <div class="form-group">
    <!-- Button -->
    <div class="controls">
      <button class="btn btn-success">Создать</button>
    </div>
  </div>
</form>

