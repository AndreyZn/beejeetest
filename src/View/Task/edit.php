<form class="form-horizontal col-md-8" id="add_task" data-toggle="validator"
      role="form" action='' method="POST">
  <div id="legend">
    <legend class="">Редактировать задачу</legend>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="form-group">
        <label for="content">Текст задачи</label>
        <textarea class="form-control" id="text" name="text"
                  placeholder="Введите текст" rows="7" cols="60"
                  required><?php if (isset($textOld)) {
            echo $textOld;
          } ?></textarea>
      </div>
    </div>
  </div>
  <div class="form-group">
    <!-- Button -->
    <div class="controls">
      <button class="btn btn-success">Сохранить</button>
    </div>
  </div>
</form>
