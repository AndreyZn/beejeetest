<!-- Content -->
<?php if (!empty($tasks)): ?>
  <?php foreach ($tasks as $task): ?>
    <div class="task-item">
      <div class="thumbnail">
        <div class="caption">
          <div class="card-text">
            <small class="col-md-6 text-muted text-right">Имя
              пользователя: <b><?= $task->user_name; ?></b></small>
            <small class="col-md-6 text-muted text-left">email:
              <b><?= $task->email; ?></b></small>
          </div>
          <h3>Задача №: <?= $task->id; ?></h3>
          <p><?= $task->text; ?></p>
          <p class="card-text"><small class="text-muted">Статус:
              <b><?= $task->status; ?></b></small></p>
          <p class="card-text"><small
              class="text-muted text-danger"><b><?php if ($_SESSION['user']) {
                  echo $task->update_adm;
                } ?></b></b></small></p>
          <div class="text-center">
            <?php if ($_SESSION['user']['role'] == 'admin'): ?>
              <div class="checkbox">
                <label><input id="ch-status-<?= $task->id; ?>"
                              class="change-status-link"
                              data-id="<?= $task->id; ?>" type="checkbox"
                              value="" <?php if ($task->status == 'выполнено') {
                    echo 'checked';
                  } ?> >Выполнено</label>
              </div>
              <a href="/task/edit?id=<?= $task->id; ?>" class="btn btn-primary"
                 role="button">Редактировать</a>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
  <!-- Pagination -->
  <div class="text-center">
    <?php if ($pagination->countPages > 1): ?>
      <?= $pagination; ?>
    <? endif; ?>
  </div>
  <!-- End pagination -->
<?php else: ?>
  <div class="task-item">
    <div class="thumbnail">
      <div class="caption">
        <div class="card-text">
          <h3>Список задач пуст</h3>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End content -->


