<!-- Form -->
<form class="form-horizontal col-md-8" id="signup" data-toggle="validator"
      role="form" action='' method="POST">
  <fieldset>
    <div id="legend">
      <legend class="">Войти</legend>
    </div>
    <div class="form-group has-feedback">
      <!-- Login -->
      <label class="control-label" for="login">Логин</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input type="text" id="login" name="login" placeholder="Логин"
               class="form-control" data-maxlength="255" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
      <!-- Password-->
      <label class="control-label" for="password">Пароль</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <input type="password" id="password" name="password"
               placeholder="Пароль" class="form-control" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
      <!-- Button -->
      <div class="controls">
        <button class="btn btn-success">Войти</button>
      </div>
    </div>
  </fieldset>
</form>
<!-- End form -->
