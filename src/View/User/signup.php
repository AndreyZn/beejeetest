<!-- Form -->
<form class="form-horizontal col-md-8" id="signup" data-toggle="validator"
      role="form" action='' method="POST">
  <fieldset>
    <div id="legend">
      <legend class="">Регистрация</legend>
    </div>
    <div class="form-group has-feedback">
      <!-- Login -->
      <label class="control-label" for="login">Логин</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input type="text" id="login" name="login" placeholder="Логин"
               value="<?= isset($_SESSION['form_data']['login']) ? htmlspecialchars($_SESSION['form_data']['login']) : ''; ?>"
               class="form-control" data-maxlength="255" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
      <!-- Username -->
      <label class="control-label" for="name">Имя</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input type="text" id="name" name="name" placeholder="Имя"
               value="<?= isset($_SESSION['form_data']['name']) ? htmlspecialchars($_SESSION['form_data']['name']) : ''; ?>"
               class="form-control" data-maxlength="255" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
      <!-- E-mail -->
      <label class="control-label" for="email">E-mail</label>
      <div class="input-group">
        <span class="input-group-addon"><i
            class="glyphicon glyphicon-envelope"></i></span>
        <input type="email" id="email" name="email" placeholder="email"
               class="form-control" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
      <!-- Password-->
      <label class="control-label" for="password">Пароль</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <input type="password" id="password" name="password"
               placeholder="Пароль"
               data-error="Пароль должен включать не менее 3 символов"
               data-minlength="3" class="form-control" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
      <!-- Button -->
      <div class="controls">
        <button class="btn btn-success">Зарегистрироваться</button>
      </div>
    </div>
  </fieldset>
</form>
<!-- End form -->

<?php if (isset($_SESSION['form_data'])) {
  unset($_SESSION['form_data']);
} ?>