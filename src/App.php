<?php


namespace src;

/**
 * Class App
 */
class App {

  /**
   * Properties container
   *
   * @var \src\TSingleton
   */
  public static $app;

  /**
   * App constructor.
   */
  public function __construct() {
    $query = trim($_SERVER['REQUEST_URI'], '/');
    session_start();
    self::$app = Registry::instance();
    $this->getParams();
    new ErrorHandler();
    Router::dispatch($query);
  }

  /**
   * Write parameters in properties container
   */
  protected function getParams() {
    $params = require_once CONFIG . '/params.php';

    if (!empty($params)) {
      foreach ($params as $k => $v) {
        self::$app->setProperty($k, $v);
      }
    }
  }

}