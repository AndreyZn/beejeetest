<?php


namespace src\Model;


use src\Db;
use Valitron\Validator;

/**
 * Class BaseModel
 *
 * @package src\Model
 */
class BaseModel {

  public $attributes = [];

  public $errors = [];

  public $rules = [];

  public function __construct() {
    Db::instance();
  }

  /**
   * @param $data
   */
  public function load($data) {
    foreach ($this->attributes as $name => $value) {
      if (isset($data[$name])) {
        $this->attributes[$name] = $data[$name];
      }
    }
  }

  /**
   * @param $table
   *
   * @return int|string
   * @throws \RedBeanPHP\RedException\SQL
   */
  public function save($table) {
    $tbl = \R::dispense($table);

    foreach ($this->attributes as $name => $value) {
      $tbl->$name = $value;
    }

    return \R::store($tbl);
  }

  /**
   * @param $data
   *
   * @return bool
   */
  public function validate($data) {
    //Validator::langDir('');
    Validator::lang('ru');
    $validator = new Validator($data);
    $validator->rules($this->rules);

    if ($validator->validate()) {
      return TRUE;
    }
    $this->errors = $validator->errors();
    return FALSE;
  }

  /**
   * Get errors
   */
  public function getErrors() {
    $errors = '<ul>';
    foreach ($this->errors as $error) {
      foreach ($error as $item) {
        $errors .= "<li>$item</li>";
      }
    }

    $errors .= '</ul>';
    $_SESSION['error'] = $errors;
  }

}