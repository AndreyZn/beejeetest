<?php


namespace src\Model;


use RedBeanPHP\R;

/**
 * Class TaskModel
 *
 * @package src\Model
 */
class TaskModel extends BaseModel {

  public $attributes = [
    'text' => ''
  ];

  public $rules = [
    'required' => [
      ['text']
    ]
  ];

  /**
   * @return mixed
   */
  public function getUserId() {
    $user = R::findOne('users', 'email = ?', [$_SESSION['user']['email']]);

    if ($user) {
      return $user->id;
    }
  }

}