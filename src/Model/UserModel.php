<?php


namespace src\Model;


use RedBeanPHP\R;

/**
 * Class UserModel
 *
 * @package src\Model
 */
class UserModel extends BaseModel {

  public $attributes = [
    'login' => '',
    'name' => '',
    'password' => '',
    'email' => '',
  ];

  public $rules = [
    'required' => [
      ['login'],
      ['name'],
      ['password'],
      ['email'],
    ],
    'email' => [
      ['mail'],
    ],
    'lengthMin' => [
      ['password', 3],
    ],
    'lengthMax' => [
      ['login', 255],
      ['name', 255],
      ['email', 255],
      ['password', 255],
    ],
  ];

  /**
   * @return bool
   */
  public function checkUnique() {
    $user = R::findOne('users', 'login = ? OR email = ?', [
      $this->attributes['login'],
      $this->attributes['email'],
    ]);

    if ($user) {
      if ($user->login == $this->attributes['login']) {
        $this->errors['unique'][] = 'Этот логин уже занят.';
      }
      if ($user->email == $this->attributes['email']) {
        $this->errors['unique'][] = 'Этот email уже занят.';
      }

      return FALSE;
    }
    return TRUE;
  }

  /**
   * @param bool $isAdmin
   *
   * @return bool
   */
  public function login($isAdmin = FALSE) {
    $login = !empty(trim($_POST['login'])) ? trim($_POST['login']) : NULL;
    $password = !empty(trim($_POST['password'])) ? trim($_POST['password']) : NULL;

    if ($login && $password) {
      if ($isAdmin) {
        $user = R::findOne('users', "login = ? AND role = 'admin'", [$login]);
      }
      else {
        $user = R::findOne('users', "login = ?", [$login]);
      }

      if ($user) {
        if (password_verify($password, $user->password)) {
          foreach ($user as $k => $v) {
            if ($k != 'password') {
              $_SESSION['user'][$k] = $v;
            }
          }

          return TRUE;
        }
      }
    }
    return FALSE;
  }

}