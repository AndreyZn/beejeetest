<?php


namespace src;

/**
 * Class Registry
 *
 * @package src
 */
class Registry {

  use TSingleton;

  /**
   * @var array
   */
  public static $properties = [];

  /**
   * Set property
   *
   * @param $name
   * @param $value
   */
  public function setProperty($name, $value) {
    self::$properties[$name] = $value;
  }

  /**
   * Get property
   *
   * @param $name
   *
   * @return mixed|null
   */
  public function getProperty($name) {
    if (isset(self::$properties[$name])) {
      return self::$properties[$name];
    }

    return null;
  }

  /**
   * Get all properties
   *
   * @return array
   */
  public function getProperties() {
    return self::$properties;
  }
}