<?php


namespace src;


use RedBeanPHP\R;

/**
 * Class Db
 *
 * @package src
 */
class Db {

  use TSingleton;

  /**
   * Db constructor.
   *
   * @throws \Exception
   */
  protected function __construct() {
    $db = require_once CONFIG . '/config_db.php';

    class_alias('\RedBeanPHP\R', '\R');
    R::setup($db['dsn'], $db['user'], $db['pass']);

    if (!R::testConnection()) {
      throw new \Exception('No connection to DB', 500);
    }

    \R::freeze(TRUE);
  }

}