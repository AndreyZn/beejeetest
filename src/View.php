<?php


namespace src;

/**
 * Class View
 *
 * @package src
 */
class View {

  public $route;

  public $controller;

  public $model;

  public $view;

  public $prefix;

  public $layout;

  public $data = [];

  public $meta = [];

  /**
   * View constructor.
   *
   * @param $route
   * @param string $layout
   * @param string $view
   * @param string $meta
   */
  public function __construct($route, $layout = '', $view = '', $meta = '') {
    $this->route = $route;
    $this->controller = $route['controller'];
    $this->view = $view;
    $this->model = $route['controller'];
    $this->prefix = $route['prefix'];
    $this->meta = $meta;

    if ($layout === FALSE) {
      $this->layout = FALSE;
    }
    else {
      $this->layout = $layout ?: LAYOUT;
    }
  }

  /**
   * @param $data
   *
   * @throws \Exception
   */
  public function render($data) {
    if (is_array($data)) {
      extract($data);
    }

    $viewFile = APP . "/View/{$this->prefix}{$this->controller}/{$this->view}.php";
    if (is_file($viewFile)) {
      ob_start();
      require_once $viewFile;
      $content = ob_get_clean();
    }
    else {
      throw new \Exception("View {$viewFile} not found!", 500);
    }

    if (FALSE !== $this->layout) {
      $layoutFile = APP . "/View/layout/{$this->layout}.php";
      if (is_file($layoutFile)) {
        require_once $layoutFile;
      }
      else {
        throw new \Exception("Layout {$layoutFile} not found!", 500);
      }
    }
  }

  /**
   * @return string
   */
  public function getMeta() {
    $output = '<title>' . $this->meta['title'] . '</title>' . PHP_EOL;
    $output .= '<meta name="description" content="' . $this->meta['desc'] . '">' . PHP_EOL;
    $output .= '<meta name="keywords" content="' . $this->meta['keywords'] . '">' . PHP_EOL;

    return $output;
  }

}