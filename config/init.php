<?php

define('ROOT', dirname(__DIR__));

const DEBUG = 1;
const WWW = ROOT . '/public';
const APP = ROOT . '/src';
const CACHE = ROOT . '/tmp/cache';
const CONFIG = ROOT . '/config';
const LAYOUT = 'default';

$app_path = "http://{$_SERVER['HTTP_HOST']}";

define('PATH', $app_path);
define('ADMIN', PATH . '/admin');

require_once ROOT . '/vendor/autoload.php';